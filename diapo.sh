#!/usr/bin/env bash
# François Audirac <francois.audirac@cemea.asso.fr>
# https://ladoc.cemea.org
# Prérecquis : imagemagick, peh, omxplayer
# On doit convertir les PDF en images
# Puis afficher toutes les images
# Puis balancer les vidéos
# Et on recommence

# TODO : créer un script d'install qui fait tout : installation, lancement et configuration et mise à jour du cron
# DONE : si des dossiers sont absents de la clef USB, on les crée !
# DONE : on gère les délais entre les images (avec une variable manuelle)
# TODO : on ajoute un fichier d'explication LISEZMOI.txt et config.txt commenté à la racine de la clef.
# TODO-PDF : traiter le cas des fichiers avec extensions en majuscules
# TODO : traiter le cas des vidéos non lisibles ?
# TODO : ajouter du son en fond sonore pour les diaporamas
# TODO : Ajouter un fondu entre vidéos + temps de délais
# TODO : détecter si une URL dans le fichier de config et lancer un navigateur fixe.

# RACINE="clefUSB" # Lieu où se trouve les fichiers medias
CONFIGFILE="config.txt" # Fichier de config
DELAIURL=30 # Delai par défaut pour affichage de site
DELAIURLDEFAULT=60 # Durée en sec par défaut pour affichage d'un site
DIAPORAMA="Diaporama" #Nom du dossier du diaporama
PERMANENT="Permanent" # Nom du dossier avec fichiers permanents
CONFIGPERSO="configperso.txt" # fichier avec configuration personnalisée dans chaque dossier de medias
FONDECRANDEFAUT="fondecran.jpg" # nom par défaut du fond d'écran

PDF2JPGDIR="PDF2JPG" #Dossier d'export des PDF convertis en JPG
DEVCLEF="/dev/sda1" #lieu par défaut de la clef USB
RACINE="/media/clefUSB" #lieu de montage si non montée
DELAIIMGDEFAUT=3 # Durée d'affichage par défaut entre 2 images

function montageclef {
	# Monte la clef si ce n'est pas déjà fait
	# On cherche si le point de montage existe déjà
	pointmontagecle=`cat /etc/mtab |grep sda1|cut -f2 -d" "`
	if [ ! -z $pointmontagecle ]
	then
		export RACINE="$pointmontagecle"
		echo "Clef déjà montée sur $RACINE !"
	else
		# crée le chemin si inexistant
		echo "clef non montée !"
		# TODO : on doit tester si une clef est branchée sur sda,sdb,sdc
		if [ ! -d $RACINE ]; then mkdir -p $RACINE; fi
		# Teste si montage de clef est déjà fait, sinon monte la clef /dev/sdb1
		montclef=$(cat /etc/mtab |grep sda|wc -l)
		if [ $montclef != 0 ]
		then
			mount $DEVCLEF $RACINE
		fi
	fi
	LSCLEF=`ls -a $RACINE | sed -e "/\.$/d" | wc -l`
}


function testprog {
	# teste si les programmes nécessaires sont bien présents
	ERR=""
	if [ ! -e /usr/bin/convert ]; then ERR+="imagemagick "; fi
	if [ ! -e /usr/bin/feh ]; then ERR+="feh "; fi
	if [ ! -e /usr/bin/omxplayer ]; then ERR+="omxplayer "; fi

	if [ "$ERR" != "" ];
	then
	    echo "Il manque des programmes, on les installe !"
	    sudo apt-get -y update
	    sudo apt-get -y install $ERR
	fi
}



function testdir {
    # On teste si les dossiers existent, sinon on les crée.
    echo "Racine : $RACINE"
    if [ ! -d "$RACINE/$DIAPORAMA" ]; then mkdir "$RACINE/$DIAPORAMA"; fi
    if [ ! -e "$RACINE/$CONFIGFILE" ]; then echo "Pas de fichier config : voir documentation sur https://framagit.org/cemea/raspiscreen"; fi
}

function importconfig {
    # Importe les valeurs d'un fichier de config s'il existe
    if [ -e $RACINE/$DIAPORAMA/$CONFIGFILE ]
    then
	. $RACINE/$DIAPORAMA/$CONFIGFILE #on importe le fichier config s'il existe
	# Exemples de variables importées :
	# HEURE_ARRET, HEURE_DEBUT, HEURE_FIN, DELAI, URL, FOND_ECRAN

    fi
}


function testBlackOff {
    # Fonction qui vérifie si on a dépassé l'heure d'exctinction ou de blackout
    HEURE_EN_COURS=`date +"%H%M"`
    DEBUG echo "Test arrêt : $HEURE_EN_COURS / $HEURE_ARRET "
    # Test sur l'heure de blackout
    if  [ -n "$HEURE_FIN" ]; then
	if [ "$HEURE_EN_COURS" \> "$HEURE_FIN" ]; then
	    echo "On balance un écran noir !!!" # Mais comment ???
	    # xset s +dpms
	    xset dpms force suspend # ou off ou standby (à tester)
	    sleep 1m
	elif [ -n "$HEURE_DEBUT" ]; then
	# Test sur heure rallumage
		if [ "$HEURE_EN_COURS" \> "$HEURE_DEBUT" ]; then
	    		echo "On réactive l'écran !" # Mais comment ???
	    		#xset s off noblank
	    		xset -dpms
		fi
    	fi
    fi

    if [ -n "$HEURE_ARRET" ]; then
	if [  "$HEURE_EN_COURS" \> "$HEURE_ARRET" ]; then
	    echo "Normalement, ON ETEINT TOUT car $HEURE_EN_COURS > $HEURE_ARRET"
	    sudo shutdown
	fi
    fi

}


function wallpaperperso {
    if [ -r "$RACINE/$DIAPORAMA/$FONDECRANDEFAUT" ]; then
	nouveau_fond="$FONDECRANDEFAUT"
    fi
    if ([ -n "$FOND_ECRAN" ] && [ -r "$RACINE/$DIAPORAMA/$FOND_ECRAN" ] ); then
	echo "fichier fond : $RACINE/$DIAPORAMA/$FOND_ECRAN"
	nouveau_fond="$FOND_ECRAN"
    fi
    if [ -n "$nouveau_fond" ];
    then
	echo "Copie fond écran"
	cp "$RACINE/$DIAPORAMA/$nouveau_fond" "/home/pi/Pictures/"
	pcmanfm --set-wallpaper="/home/pi/Pictures/$nouveau_fond"
	pcmanfm --wallpaper-mode=fit
    else
	pcmanfm --set-wallpaper="/usr/share/rpd-wallpaper/raspberry-pi-logo.png"
	pcmanfm --wallpaper-mode=center
    fi
}

function lastdir {
    # On liste les dossiers et on renvoie le dossier avant le moment courant
    cd $RACINE/$DIAPORAMA
    LSDIR=`ls -Ad */`
    # DEBUG echo "Liste de dossiers triés trouvés : $LSDIR"
    DATE_EN_COURS=`date +"%y%m%d-%H%M"`
    # echo "DATE : $DATE_EN_COURS"
    DOSSIER_EN_COURS=""
    DOSSIER_SUIVANT=""
    for i in `ls -Ad */`;
    do
	if [ "$i" != "$PERMANENT" ]; then
	    if [ "$i" \< "$DATE_EN_COURS" ] && [ `find $RACINE/$DIAPORAMA/$i/ -type f|wc -l` -ne 0 ]; then
		#	if [ "$i" \< "$DATE_EN_COURS" ]; then
		DOSSIER_EN_COURS="$i"
	    else
		DOSSIER_SUIVANT="$i"
		break;
	    fi
	fi
    done
}


function convpdf {
    # Conversion de tous les PDF en images
    # dans /tmp/DOSSIER
    DOSSIER=$1
    if [ -z `grep "../" "$DOSSIER" &2>/dev/null` ];
       then
	   rm -rf "/tmp/$DOSSIER"
    fi
    mkdir -p "/tmp/$DOSSIER/$PDF2JPGDIR" # et on recrée un dossier vide
   # on convertit ensuite chaque PDF en série d'images en PDF
   listepdf=`ls -1 "$RACINE/$DIAPORAMA/$DOSSIER"*.pdf 2>/dev/null`
   listepdf+=`ls -1 "$RACINE/$DIAPORAMA/$DOSSIER"*.PDF 2>/dev/null`
   echo "liste PDF = $listepdf"
   for file in $listepdf;
   do
       # echo "File $file"
       nomfichier=$(basename $file)
       # echo  "Basename : $NOMFICHIER"
       nomsansext=${nomfichier%.*}
       # echo "rac : $RACINE"
       echo "*** Conversion PDF de $file en cours"
       convert -density 100 $file "/tmp/$DOSSIER""$PDF2JPGDIR/$nomsansext-%02d.jpg"
      echo "*** Fin de conversion dans /tmp/$DOSSIER""$PDF2JPGDIR/"
   done
}

function affImg {
    # On affiche les images données en paramètres avec feh
    if [ -z $DELAI ]; then DELAI=$DELAIIMGDEFAUT; fi
    echo "On affiche les images avec feh"
    echo "Dossier : $@"
    echo "commande : feh -Z -F -Y -q -Sname --cycle-once -D$DELAI -- $1"
    feh -Z -F -Y -q -Sname --cycle-once -D$DELAI -- "$@"
}

function affVideos {
    # Fonction qui affiche une vidéo
    # on efface le curseur
    setterm -cursor off
    # On définit le service à utiliser, ici OMXPLAYER
    SERVICE="omxplayer"
    # killall $SERVICE
    echo "On visionne : $@"
    "$SERVICE" -b -o hdmi "$@" $2 > /dev/null # omxplayer sur Raspi / local pour port analogique, sinon HDMI        
    # $SERVICE "$1" > /dev/null # omxplayer sur Raspi / local pour port analogique, sinon HDMI
    # mplayer -zoom -fs $ENTRY # on remplacer omxplayer par mplayer pour des tests sous Debian
}


function projetteMedia {
    # Projet le fichier media selon l'extension concernée
    MEDIA="$1"
    DOSSIER="$2"
	EXTENSION=`echo ${MEDIA#*.}|tr [:upper:] [:lower:]`
	echo "ext : $EXTENSION"
	echo "Dans $DOSSIER, on affiche le fichier : $MEDIA"
	# on affiche le media
	case "$EXTENSION" in
	    jpg|png|gif)
		affImg "$RACINE/$DIAPORAMA/$DOSSIER""$MEDIA"
		;;
	    pdf)
		NOMSANSEXT=${MEDIA%.*}
		affImg "/tmp/$DOSSIER""$PDF2JPGDIR/$NOMSANSEXT*"
		;;
	    mp4|m4v|avi|mpg|mov|mkv|webm)
		affVideos "$RACINE/$DIAPORAMA/$DOSSIER""$MEDIA"
		;;
	    *)
		echo "Autre : $MEDIA"
	esac
}


function affSite {
    #Affiche un site à partir de l'URL
    if [ -n "$URL" ]
    then
	chromium-browser -noerrdialogs -kiosk -incognito $URL &
	if [ -z $DELAIURL ]; then
		DELAIURL=$DELAIURLDEFAULT
	fi
	sleep $DELAIURL
	killall chromium-browser
    fi
}


function init {

    xset s off
    xset s noblank # désactivation du screensaver
    xset -dpms
    sleep 5
    # On lance toute l'initialisation une fois pour toutes, à chaque démarrage du script
    testprog # on teste la présence des programmes utiles, sinon on les installe
    montageclef # on monte la clef si pas montée
    # echo "Valeur racine : $RACINE"
    testdir # On teste la présence des dossiers utiles
    importconfig # On intègre les fichiers de config (temps...)
    wallpaperperso # On ajouter le fond d'ecran si spécifié dans la config ou si present dans le dossier
}



init # On procède aux pré-recquis : installation, montage
dossier_precedent=""
convpdf "$PERMANENT/"
projection=false
modeprod=true;

while $modeprod;
do
    testBlackOff # test si besoin de lancer le diaporama selon heures d'extinction dans config

    lastdir # On récupère le dossier le moins ancien plein
    echo "dossier en cours trouvé : $DOSSIER_EN_COURS"
    if [ "$dossier_precedent" != "$DOSSIER_EN_COURS" ] && [ -n "$DOSSIER_EN_COURS" ]; then
    	convpdf $DOSSIER_EN_COURS # On convertit les PDF en images
    fi

    echo "*** Diapos du dossier $DOSSIER_EN_COURS ***"

    if [ -e "$RACINE/$DIAPORAMA/$DOSSIER_EN_COURS" ] && [ -n "$DOSSIER_EN_COURS"  ]; then
	ls $RACINE/$DIAPORAMA/$DOSSIER_EN_COURS > /tmp/listemediasencours
	for MEDIA in `cat /tmp/listemediasencours`; do
	    projetteMedia $MEDIA "$DOSSIER_EN_COURS"
	done
	echo "Diffusion Media"
	projection=true
    fi
 
    echo "Diapos du Dossier Permament"
    if [ -e "$RACINE/$DIAPORAMA/$PERMANENT" ]; then
    	ls $RACINE/$DIAPORAMA/$PERMANENT > /tmp/listemediasperm
    	# echo "**** On a trouvé des fichiers permanents ****"
    	for MEDIA in `cat /tmp/listemediasperm`; do
    	    projetteMedia $MEDIA "$PERMANENT/"
	done
	echo "Diffusion Permanent"
    	projection=true
    fi
    dossier_precedent="$DOSSIER_EN_COURS"
    if [ ! $projection ]; then
	echo "On dort 1m"
	sleep 1m
    fi
    modeprod=true
    dateactuelle=`date +%H%M`
    echo "Il est exactement : $dateactuelle !"
    echo " et on s'arrête à $HEURE_FIN"

done

echo "Fin du diaporama !"


