# RaspberryScreen

L'objectif de ce mini projet est de proposer un écran de diffusion simple et pratique pour des néophytes qui diffuse des informations en boucle.

## Matériel et fonctionnement

* Raspberry avec système Raspian installé, si possible avec le script préinstallé
* une clef USB avec du contenu : Images, PDF, Vidéos, (sons ?)

## Mise en place

On copie le script diapo.sh dans le /usr/local/bin
on lui applique les droits qui vont bien : chmod +x /usr/local/bin/diapo.sh
Et on lance le script au démarrage de la sessions en rajoutant dans :

    .config/lxsessions/LXDE-pi/autostart

On rajoute la ligne :

    @/usr/local/bin/diapo.sh

# En test, à tester
Créer un Dossier **Diaporama** à la racine de la clef
Placer un fichier de configuration nommé *config.txt* dans le dossier **Diaporama** de la clef USB avec les valeurs actuellement gérées :

    #!/bin/sh
    DELAI=5 # valeurs en secondes de la durée d'affichage d'une diapo
    # URL= # Adresse d'un site à afficher en plein écran
    # DELAIURL= #Délai d'affichage du site (60 sec par défaut)
    FOND_ECRAN=  # nom de l'image avec lieu depuis la racine de la clef
    HEURE_ARRET=2321 # pour exctinction à 23h21
    HEURE_DEBUT=0915 # pour affichage à 9h15
    HEURE_FIN=1824 # pour ecran noir à 18h24

## Utilisation

1. On commence avec le Raspberry éteint
2. On branche la clef USB avec le contenu prévu
3. On allume le raspiscreen
4. Ca diffuse en boucle le contenu jusqu'à extinction (programmée)

## Idées et pistes à venir

* Ajouter des transitions ?
* Prévoir un fichier de config pour paramétrage fin ?
* Faut-il prévoir un fond sonore si diaporamas en boucle ?
* Heure de mise en route et d'extinction, pour ne pas le faire en direct
* D'autres idées ?

### Pistes pour améliorer Raspberry Slideshow

https://www.binaryemotions.com/digital-signage/raspberry-slideshow/

* heure d'allumage, extinction ou mise en veille de l'écran
* copier fichiers de config de la clef dans le raspi 
* Démonter la clef une fois les fichiers copiés
* Refresh USB si clef USB inséré en cours de démarrage
* Créer une interface web pour administration ()
